UBUNTU_VERSION=24.04
APACHE_VERSION=2.4
PHP_VERSION_5=5.6
PHP_VERSION_7=7.4
PHP_VERSION_8=8.4
IMG=registry.gitlab.com/jhmdocker/image/apache

all: apache-base apache-php5 apache-php7 apache-php8 apache-wp-php5 apache-wp-php7 apache-wp-php8

apache-base:
	docker build --pull $(BUILD_OPTS) -t $(IMG):$(APACHE_VERSION) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		--build-arg APACHE_VERSION=$(APACHE_VERSION) \
		apache-base

apache-php5: apache-base
	docker build --pull $(BUILD_OPTS) -t $(IMG):php$(PHP_VERSION_5) \
		--build-arg APACHE_VERSION=$(APACHE_VERSION) \
		--build-arg PHP_VERSION=$(PHP_VERSION_5) \
		apache-php

apache-php7: apache-base
	docker build --pull $(BUILD_OPTS)  -t $(IMG):php$(PHP_VERSION_7) \
		--build-arg APACHE_VERSION=$(APACHE_VERSION) \
		--build-arg PHP_VERSION=$(PHP_VERSION_7) \
		apache-php

apache-php8: apache-base
	docker build --pull $(BUILD_OPTS)  -t $(IMG):php$(PHP_VERSION_8) \
		--build-arg APACHE_VERSION=$(APACHE_VERSION) \
		--build-arg PHP_VERSION=$(PHP_VERSION_8) \
		apache-php

apache-wp-php5: apache-php5
	docker build --pull $(BUILD_OPTS)  -t $(IMG):wp-php$(PHP_VERSION_5) \
		--build-arg APACHE_VERSION=$(APACHE_VERSION) \
		--build-arg PHP_VERSION=$(PHP_VERSION_5) \
		apache-wp

apache-wp-php7: apache-php7
	docker build --pull $(BUILD_OPTS)  -t $(IMG):wp-php$(PHP_VERSION_7) \
		--build-arg APACHE_VERSION=$(APACHE_VERSION) \
		--build-arg PHP_VERSION=$(PHP_VERSION_7) \
		apache-wp

apache-wp-php8: apache-php8
	docker build --pull $(BUILD_OPTS)  -t $(IMG):wp-php$(PHP_VERSION_8) \
		--build-arg APACHE_VERSION=$(APACHE_VERSION) \
		--build-arg PHP_VERSION=$(PHP_VERSION_8) \
		apache-wp

push:
	docker push $(IMG):$(APACHE_VERSION)

	docker push $(IMG):php$(PHP_VERSION_5)
	docker push $(IMG):php$(PHP_VERSION_7)
	docker push $(IMG):php$(PHP_VERSION_8)

	docker push $(IMG):wp-php$(PHP_VERSION_5)
	docker push $(IMG):wp-php$(PHP_VERSION_7)
	docker push $(IMG):wp-php$(PHP_VERSION_8)
