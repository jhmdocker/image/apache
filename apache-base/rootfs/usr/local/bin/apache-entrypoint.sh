#!/bin/bash

if [[ "$APACHE_RUN_GROUP" != "" ]]; then
  groupmod -g $APACHE_RUN_GROUP www-data
fi

if [[ "$APACHE_RUN_USER" != "" ]]; then
  usermod -u $APACHE_RUN_USER www-data
fi

exec /usr/sbin/apache2ctl -D FOREGROUND "$@"
